import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { BooksComponent } from './books.component';
import { BookDetailComponent } from './book-detail.component';
import { BookService } from './book.service';
import { DashboardComponent } from './dashboard.component';

import { AppRoutingModule } from './app-routing.module';

import { MdToolbarModule } from '@angular2-material/toolbar';
import { MdButtonModule } from '@angular2-material/button';
import { MdCardModule } from '@angular2-material/card';
import { MdRippleModule } from '@angular2-material/core';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        AppRoutingModule,
        MdButtonModule,
        MdCardModule,
        MdToolbarModule
    ],
    declarations: [
        AppComponent,
        DashboardComponent,
        BooksComponent,
        BookDetailComponent
    ],
    providers: [BookService],
    bootstrap: [AppComponent]
})
export class AppModule { }
