import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Book } from './book';
import { BookService } from './book.service';

@Component({
    selector: 'dashboard',
    template: require('./dashboard.component.html'),
    styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
    books: Book[] = [];
    constructor(
        private router: Router,
        private bookService: BookService) { }

    ngOnInit(): void {
        this.bookService.getBooks().then(books => this.books = books.slice(1, 5));
    }

    gotoDetail(book: Book): void {
        let link = ['/detail', book.id];
        this.router.navigate(link);
    }
}
