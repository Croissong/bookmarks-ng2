var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var CompressionPlugin = require("compression-webpack-plugin");

var _PROD = process.env.NODE_ENV === 'prod';

var plugins = [
  new HtmlWebpackPlugin({
    template: './app/index.ejs',
    baseUrl: _PROD ? '/bookmarks-ng2/' : '/'
  })
];

if (_PROD) {
  plugins = plugins.concat([
    new webpack.optimize.UglifyJsPlugin({
      compress: true,
      mangle: true
    }),
    new CompressionPlugin({
      asset: "[path].gz[query]",
      algorithm: "gzip",
      test: /\.js$|\.html$|\.css$/,
      threshold: 10240,
      minRatio: 0.8
    })
  ]);
}

module.exports = {
  entry: './app/app.main.ts',
  output: {
    path: __dirname + '/dist',
    filename: 'bundle.js'
  },
  module: {
    loaders: [
      {
	test: /\.ts$/,
	exclude: /node_modules/,
	loaders: ['ts', 'angular2-template-loader']
      },
      {
	test: /\.html$/,
	exclude: /node_modules/,
	loader: 'raw'
      },
      {
        test:   /\.css$/,
        loaders: ['raw', 'postcss']
      }
    ]
  },
  postcss: function () {
    return [require('postcss-cssnext')];
  },
  resolve: {
    extensions: ['', '.js', '.ts', '.css']
  },
  plugins,
  devtool: process.env.NODE_ENV === 'prod' ? '' : '#inline-source-map',
  devServer: {
    historyApiFallback: true
  }
}
