import { Book } from './book';

export const BOOKS: Book[] = [
    { id: 1, title: 'Book01' },
    { id: 2, title: 'Book02' },
    { id: 3, title: 'Book03' },
    { id: 4, title: 'Book04' },
    { id: 5, title: 'Book05' },
    { id: 6, title: 'Book06' },
    { id: 7, title: 'Book07' },
    { id: 8, title: 'Book08' },
    { id: 9, title: 'Book09' },
    { id: 10, title: 'Book10' },
    { id: 11, title: 'Book11' },
    { id: 12, title: 'Book12' }
];
