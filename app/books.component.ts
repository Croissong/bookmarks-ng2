import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Book } from './book';
import { BookService } from './book.service';

@Component({
    selector: 'books',
    templateUrl: './books.component.html',
    styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {
    books: Book[];
    selectedBook: Book;

    constructor(
        private router: Router,
        private bookService: BookService
    ) { }

    ngOnInit(): void {
        this.getBooks();
    }

    onSelect(book: Book): void {
        this.selectedBook = book;
    }

    getBooks(): void {
        this.bookService.getBooks().then(books => this.books = books);
    }

    gotoDetail(book: Book): void {
        this.router.navigate(['/detail', this.selectedBook.id]);
    }
}
