import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';

import { BookService } from './book.service';
import { Book } from './book';

import { MdButton } from '@angular2-material/button';

@Component({
    selector: 'book-detail',
    templateUrl: './book-detail.component.html'
})
export class BookDetailComponent implements OnInit {
    @Input() book: Book;

    constructor(
        private bookService: BookService,
        private route: ActivatedRoute,
        private location: Location
    ) { }

    ngOnInit(): void {
        this.route.params.forEach((params: Params) => {
            let id = +params['id'];
            this.bookService.getBook(id).then(book => this.book = book);
        });
    }

    goBack(): void {
        this.location.back();
    }
};
