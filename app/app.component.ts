import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'app',
    templateUrl: './app.component.html',
    styles: [require('normalize-css/normalize.css')],
    encapsulation: ViewEncapsulation.None
})
export class AppComponent {
    title = "AppComponent"
}
